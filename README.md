# Travail pratique 2 : Déplacement d'un agent intelligent

Dans ce deuxième travail pratique, vous devez implémenter un "agent
intelligent" qui explore un ensemble de pièces (*rooms*) liées par des
téléporteurs (*portals*).

Le travail doit être réalisé **seul** et doit être remis au plus tard le **26
avril 2018**, à 23h59, après quoi une pénalité de **2%** par heure de retard
sera imposée.

## Objectifs spécifiques

Les principaux objectifs visés sont les suivants :

- Se **familiariser** avec le paradigme de programmation **logique** par
  l'intermédiaire du langage Prolog;
- Modéliser des contraintes à l'aide d'**expressions logiques**;
- Exploiter la stratégie de **branchement** et d'**évaluation progressive**
  pour résoudre un problème d'optimisation.
- Simuler le comportement d'un **agent intelligent** évoluant dans un monde
  décrit à l'aide de **contraintes**.

## Mise en contexte

Vous devez modéliser et implémenter un agent intelligent qui évolue dans un
monde composé de *pièces*, connectées par des *téléporteurs*.

Il existe trois types de pièces, caractérisées par leur forme: `square(n)`,
`diamond(n)` et `triangle(n)`, où `n` indique la dimension de la pièce. La
figure ci-bas illustre les pièces `square(4)`, `diamond(5)` et `triangle(7)`.
Notez que les dimensions des pièces de forme `diamond` et `triangle` doivent
être impaires, mais la dimension d'une pièce de type `square` peut être paire
ou impaire. De plus, la pièce de type `triangle` est toujours orientée comme
dans l'image, c'est-à-dire que la "pointe" du triangle (en position `(4,4)`
dans l'image) est dirigée vers le bas.

![](images/types-pieces.png)

Pour aller d'une pièce à l'autre, il faut utiliser un *téléporteur* (en
anglais, *portal*). Dans la figure suivante, on trouve trois téléporteurs,
identifiés par des cercles noirs et portant les numéros 1, 2 et 3. Par exemple,
il est possible d'utiliser le téléporteur 1 en se déplaçant de la position
`(2,1)` de la pièce `room1`, pour aller jusqu'à la position `(1,3)` de la pièce
`room2`. Bien entendu, il est aussi possible d'utiliser un téléporteur en sens
inverse: ainsi, on pourrait aussi aller de la pièce `room2` à la pièce `room1`
en utilisant le téléporteur 1.

![](images/monde.png)

Par défaut, les seuls déplacements possibles se font à l'horizontale et à la
verticale. Autrement dit, si on se trouve dans une pièce en position `(x,y)`,
alors les seules cases accessibles sont `(x+1,y)`, `(x-1,y)`, `(x,y+1)` et
`(x,y-1)`, en autant évidemment que les positions soient valides (à l'intérieur
de la pièce courante).

En plus de naviguer de pièce en pièce à l'aide de téléporteurs, vous devez être
en mesure de le faire en respectant une *contrainte de temps*. Plus
précisément, vous devez tenir compte des éléments suivants:

- Se déplacer d'une case à une autre prend **1 unité de temps**;
- L'utilisation d'un téléporteur prend **3 unités de temps**.

Par exemple, supposons que l'agent intelligent démarre en position `(4,1)` de
la pièce `room1` et souhaite se rendre en position `(3,4)` de la pièce `room3`.
Alors il est possible de le faire en **13 unités** (**7** flèches vertes +
**2** téléporteurs à **3** unités chacun) de temps, comme l'illustre l'image
plus bas (la solution est illustrée en vert):

![](images/solution.png)

Finalement, il est possible qu'une ou plusieurs pièces contiennent un ou
plusieurs objets spéciaux appelés **diagonaliseurs**. Ces objets aux propriétés
magiques remarquables permettent à votre agent de se déplacer également en
diagonal en utilisant le même temps (**1 unité de temps**) que s'il se
déplaçait à l'horizontale et à la verticale seulement. Il peut donc, dans
certains cas, être avantageux de "perdre" un peu de temps pour aller récupérer
un diagonaliseur afin d'accélérer les déplacements subséquents. Par exemple, si
on reprend l'exemple précédent et qu'on place un diagonaliseur en position
`(3,2)` de la pièce `room1` (le nuage orange dans l'image ci-bas), alors on
peut se rendre du point `S` au point `E` en **10 unités** de temps (**7**
flèches vertes + **1** téléporteur à **3** unités).

![](images/diagonaliseur.png)

## Réalisation du travail

Rendez-vous sur https://gitlab.com/ablondin/inf2160-hiv2018-tp2, où se trouvent
les différents fichiers. Comme pour le premier travail pratique, votre projet
doit être un clone (*fork*) **privé** de ce projet, afin de faciliter autant la
remise que la synchronisation.

Les étapes de développement suivantes sont suggérées pour mener à terme votre
travail pratique.

### Représentation des pièces

La première étape consiste à bien définir les positions possibles dans une
pièce donnée selon sa forme. Dans mon cas, j'ai utilisé un prédicat
```prolog
in_shape(Pos, Shape)
```
qui indique si la position `Pos` se trouve à l'intérieur de la forme `Shape`.

### Algorithme de Dijkstra

Ensuite, assurez-vous d'avoir bien compris comment fonctionne l'algorithme de
Dijkstra, qui permet de calculer un plus court chemin dans un graphe orienté.
Cet algorithme a été abordé au cours du
[laboratoire 10](https://gitlab.com/ablondin/inf2160-exercices/blob/master/labo10/README.md)
où vous pouvez retrouver le code source. Dans ma solution, j'utilise le contenu
du fichier `dijkstra.pl` sans modification, donc n'hésitez pas à le recopier
entièrement dans votre travail (c'est tout à fait permis!).

### Représentation du graphe d'états

La prochaine étape, très importante, consiste à réfléchir à votre façon de
représenter le graphe d'états de l'agent intelligent. Ce graphe est défini
comme suit:

- Un **état** est une structure Prolog `state(Pos, Room, Item)`, où `Pos`
  indique la position de l'agent, `Room` la pièce où il se trouve et `Item` est
  une valeur qui vaut `diagonalizer` ou `nothing` selon que l'agent a ou non en
  sa possession le diagonaliseur.
- Un **arc** est une structure Prolog `arc(S1, S2, T)` où `S1` et `S2` sont des
  états et `T` est une quantitét de temps qui indique combien de temps est
  "consommé" si on passe de l'état `S1` à l'état `S2`.

Ce graphe d'états permet de donner une "intelligence" à l'agent.  Évidemment,
il faut tenir compte de tous les déplacements possibles:

1. D'une case à une autre dans une pièce donnée;
2. D'une pièce à une autre par l'intermédiaire d'un téléporteur;
3. On peut aussi utiliser un arc pour modéliser le moment où l'agent prend
   possession d'un diagonaliseur (la position et la pièce ne changent pas,
   juste le paramètre `Item`, et le coût en temps de cette transition est
   simplement `0`)!

### Calcul de tous les chemins

Il est important d'observer que l'algorithme de Dijkstra défini plus haut
permet de calculer **un seul** chemin optimal, alors que, dans notre cas, nous
souhaitons calculer **tous les chemins** qui utilisent un temps ne dépassant
pas une certainte limite.

Heureusement, il est possible d'exploiter l'algorithme de Dijkstra pour tous
les calculer. Plus précisément, il s'agit d'utiliser les stratégies suivantes:

- Si l'état courant de l'agent est tel que la plus courte distance entre son
  état et l'état final souhaité est **plus grande** que le temps qu'il lui
  reste, alors on doit abandonner l'exploration (ça ne sert à rien de
  continuer!);
- Sinon, ça veut dire qu'il existe au moins un chemin. On peut donc se déplacer
  le long de n'importe quel arc (éventuellement, par **marche-arrière**, on
  pourra tous les essayer) et ensuite appeler récursivement le prédicat pour
  calculer un nouveau chemin vers le but.

Une fois que cette étape est complétée, il ne reste plus qu'à bien nettoyer
votre code, le documenter et réviser votre fichier README pour la remise.

## Tests automatiques

À tout moment, vous pouvez vérifier si votre programme fonctionne en lançant
une suite de tests. Pour cela, il suffit de lancer la commande
```
make test
```
Un dernier test qui prend un temps un peu plus long à s'exécuter peut aussi
être lancé avec
```
make longtest
```

Notez que l'efficacité de votre programme sera prise en considération. Ainsi,
les trois premiers tests devraient s'exécuter très rapidement, alors que le
troisième devrait prendre quelques dizaines de secondes.

## Documentation

Votre programme devrait tenir dans un seul fichier nommé `tp2.pl`. Vous devrez
documenter tous les prédicats importants de votre programme en suivant le
standard [PlDoc](http://swi-prolog.org/pldoc/package/pldoc.html).

Minimalement, vous devez

1. Documenter l'en-tête du module, en indiquant son nom, l'auteur et une courte
   description.
2. Documenter l'en-tête des prédicats, en précisant le comportement attendu du
   prédicat, selon que l'appel est fait de façon **déterministe** ou **non
   déterministe**.

## Fichier `README.md`

Comme pour le premier travail pratique, vous devez accompagner votre remise
d'un fichier `README.md` qui indique des informations pertinentes sur l'état de
votre projet, ses dépendances, son fonctionnement, etc.

## Contraintes

Afin d'éviter des pénalités, il est important de respecter les contraintes
suivantes:

- Votre projet doit être un clone (*fork*) **privé** du projet
  https://gitlab.com/ablondin/inf2160-hiv2018-tp2. L'adjectif **privé** est
  très important si vous ne voulez pas que d'autres étudiants accèdent à votre
  solution (**pénalité de 50%**).
- Vous devez donner accès en mode `Developer` à l'utilisateur `ablondin` avant
  la date de remise (**pénalité de 50%**).
- Votre programme doit **compiler** lorsqu'on entre la commande `make`
  (**pénalité de 100%**).

## Compléments sur l'utilisation du logiciel Git

Si vous avez commencé à travailler sur le projet et que j'ai apporté des
modifications au dépôt public, il est possible de les récupérer facilement à
l'aide de Git.

J'utiliserai la terminologie suivante:
- *local* est votre copie locale du dépôt sur laquelle vous travaillez. Il peut
  s'agir de votre machine personnelle, d'une machine dans les laboratoires de
  l'UQAM ou encore du serveur Java (si vous travaillez en connexion SSH).
- *origin* est le dépôt obtenu sur GitLab après avoir fait un *fork* (clone).
- *upstream* est le dépôt public qui appartient à l'utilisateur `ablondin`
  (moi-même).

### Établir le lien entre *local* et *upstream*

En principe, vous avez actuellement déjà un lien entre le dépôt *local* et le
dépôt *origin*, qui a été établi lorsque vous avez cloné le projet sur votre
machine personnelle (ou sur le serveur Java). Vous pouvez voir comment ce lien
est établi en entrant la commande
```sh
git remote -v
```
qui indique qu'il y a un dépôt distant (en anglais, *remote*) qui s'appelle
*origin* et qui est votre sauvegarde personnelle sur GitLab. Vous devez donc
appeler un deuxième dépôt distant qui s'appellera *upstream*. Pour cela, il
suffit d'entrer la commande
```sh
git remote add upstream https://gitlab.com/ablondin/inf2160-hiv2018-tp1.git
```
pour établir le lien avec mon dépôt. Notez que le protocole ici est HTTPS et
non SSH, car vous ne pousserez pas de modifications sur mon dépôt, vous allez
seulement en récupérer.

Si jamais vous vous êtes trompés en entrant la commande, il est toujours
possible de supprimer un dépôt distant (tapez la commande `git remote --help`
pour plus de détails).

### Récupérer la version la plus à jour

Avant de récupérer les modifications, assurez-vous que votre dépôt est
*propre*, c'est-à-dire que le résultat de la commande
```sh
git status
```
indique qu'il n'y a aucune modification en cours qui n'a pas encore été
*committée*.

Je suppose ici que vous n'êtes pas trop familiers avec les branches (sinon,
vous n'auriez sans doute pas besoin de lire cette partie). Comme ce cours ne
porte pas sur le logiciel Git spécifiquement, vous pouvez simplement récupérer
les modifications en entrant la commande
```sh
git pull upstream master
```
Attention, il est possible que cette commande entraîne un conflit. Si c'est le
cas, je vous encourage à lire un tutoriel qui explique comment résoudre un
conflit (c'est une activité très commune quand on utilise Git). Par exemple,
vous pouvez regarder [ce
lien](http://alainericgauthier.com/git/gerer_les_conflits_de_fusion).

### Capsules vidéos

Si vous préférez avoir des explications plus "visuelles" de l'utilisation de
Git, vous pouvez accéder à trois capsules vidéos que j'ai tournées il y a
quelques années, dans lesquelles j'expliquais aux étudiants comment faire
certaines modifications sous Git.  Noter qu'elles sont un peu plus avancées que
celle que j'explique plus haut et que vous devrez adapter certaines parties à
ce travail pratique.  Les vidéos se trouvent
[ici](https://uqam.hosted.panopto.com/Panopto/Pages/Sessions/List.aspx?folderID=1a2b1115-e453-4c6d-b972-a87a0042b8b2).

### Intégration continue

Il est possible d'activer l'intégration continue à votre dépôt. Pour cela, il
suffit d'ajouter un fichier nommé `.gitlab-ci.yml` et de le placer à la racine
de votre projet.

L'intégration continue permet de tester de façon automatique votre code chaque
fois que vous poussez des modifications sur le dépôt. Pour plus d'informations,
veuillez consulter la
[documentation](https://about.gitlab.com/features/gitlab-ci-cd/).

Notez que cet ajout est optionnel lors de la remise du travail pratique.

## Remise

Votre travail devra être remis au plus tard le **26 avril 2018** à **23h59**. À
partir de minuit, une pénalité de **2 points par heure** de retard sera
appliquée.

La remise se fait **obligatoirement** par l'intermédiaire de la plateforme
[GitLab](https://about.gitlab.com).  **Aucune remise par courriel ne sera
acceptée** (le travail sera considéré comme non remis).

Les travaux seront corrigés sur le serveur Java. Vous devez donc vous assurer
que votre programme fonctionne **sans modification** sur celui-ci.

## Barème

Les critères suivants seront pris en compte dans l'évaluation.

- **Fonctionnalité (120 points)**: Les prédicats ont le comportement auquel on
  s'attend. La fonctionnalité sera vérifiée à l'aide de tests automatiques.
  Plus précisément, ce sont les fonctionnalités suivantes qui seront évaluées:

    * **(30 points)** La forme des pièces (carrée, losange ou triangle) est
      gérée correctement.
    * **(10 points)** L'algorithme de Dijkstra est bien implémenté et bien
      employé;
    * **(40 points)** Le graphe d'états est correctement représenté et tient
      compte de toutes les transitions possibles, incluant celles avec les
      téléporteurs et le diagonaliseur.
    * **(40 points)** Tous les chemins possibles sont générés exactement une
      fois. Autrement dit, aucun chemin n'est manqué et il n'y a pas de chemin
      généré plus d'une fois.

- **Style de programmation (40 points)**: Le style de programmation sera pris
  en considération. Voici quelques aspects qui seront évalués:
  
    * Tous les prédicats sont documentés selon le [standard recommandé](http://swi-prolog.org/pldoc/package/pldoc.html);
    * Le code est bien indenté;
    * Le code ne contient pas de caractères problématiques (encodage UTF8);
    * Éviter les caractères de tabulations qui influencent l'indentation selon
      l'éditeur de texte utilisé;
    * Le code est bien aéré autour des opérateurs et des délimiteurs.
    * Le code ne contient pas de longues lignes;
    * Le code ne redéfinit pas des fonctions déjà implémentées dans les
      fonctions prédéfinies en Prolog.
    * L'implémentation est aussi courte que possible;
    * L'implémentation est lisible.

- **Fichier README (40 points)**: 

    * Le contenu du fichier est complet et exhaustif, selon les sections
      proposées.
    * Il exploite correctement le format Markdown (bouts de code, pas d'italique
      et de gras partout, découpage en sections, utilisation de listes à puces);
    * Il ne contient pas de fautes d'orthographe, de frappe ou d'anglicismes (à
      moins que les mots en anglais soient identifiés en italique).
